/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.poc;

import com.mycompany.dcoffee.dao.PayrollDao;
import com.mycompany.dcoffee.model.Payroll;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author nasan
 */
public class TestPayrollDao {

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateFormat = "2024-05-05 18:00";
        Date date = df.parse(dateFormat);
        Payroll payroll = new Payroll(date, 9000, 0, 9000);
        PayrollDao payrollDao = new PayrollDao();

        // Test save
        payrollDao.save(payroll);

        // Test get
        Payroll updatePayroll = payrollDao.get(8);
        System.out.println(updatePayroll);

        // Test update
        updatePayroll.setTotal(7000);
        payrollDao.update(updatePayroll);

        // Test delete
        payrollDao.delete(payrollDao.get(9));
    }

}
