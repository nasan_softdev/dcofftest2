/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.poc;

import com.mycompany.dcoffee.dao.IngredientDao;
import com.mycompany.dcoffee.model.Ingredient;

/**
 *
 * @author Acer
 */
public class TestIngredientDao {

    public static void main(String[] args) {
        Ingredient ingredient = new Ingredient("สมโจ๊ะ", 5, "กิโลกรัม", 3);
        IngredientDao ingredientDao = new IngredientDao();

        //save
        ingredientDao.save(ingredient);

        //update
        Ingredient updateingredient = ingredientDao.get(8);
        updateingredient.setQuantity(4);
        ingredientDao.update(updateingredient);

        //delete
        ingredientDao.delete(ingredientDao.get(8));

    }

}
