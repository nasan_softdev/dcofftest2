/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.poc;

import com.mycompany.dcoffee.dao.MemberDao;
import com.mycompany.dcoffee.model.Member;

/**
 *
 * @author Pinkz7_
 */
public class TestMemberDao {
    public static void main(String[] args) {
        Member member = new Member("ธนาธวร","คงควรคอย","0123456789",0);
        MemberDao memberDao = new MemberDao();
        
        // Test save
//        memberDao.save(member);
        
        // Test get
        Member updateMember = memberDao.get(5);
//        System.out.println(updateMember);
        
        // Test update
//        updateMember.setName("Ahhhhhhhhh");
//        updateMember.setSuername("Haaaaaaaaaa");
       memberDao.update(updateMember);
        
//        // Test delete
        memberDao.delete(memberDao.get(5));
    }
    
}
