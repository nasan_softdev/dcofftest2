/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.poc;

import com.mycompany.dcoffee.dao.ProductDao;
import com.mycompany.dcoffee.model.Product;

/**
 *
 * @author PC Sakda
 */
public class TestProductDao {
    public static void main(String[] args) {
    Product product = new Product("น้ำส้วม",50,"orangejuice.jpg");
    ProductDao productDao = new ProductDao();
    productDao.save(product);
    
        // Test get
        Product updateProduct = productDao.get(11);
        System.out.println(updateProduct);
        // Test update
        updateProduct.setName("น้ำส้วมอดงโซดา");
        productDao.update(updateProduct);
        // Test Delete
        productDao.delete(productDao.get(11));
    }
}
