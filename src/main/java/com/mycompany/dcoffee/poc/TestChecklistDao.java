/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.poc;

import com.mycompany.dcoffee.dao.ChecklistDao;
import com.mycompany.dcoffee.model.Checklist;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author PC Sakda
 */
public class TestChecklistDao {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateFormat = "2022-11-29 15:30";
        Date date = df.parse(dateFormat);
        
        Checklist checklist = new Checklist(date);
        ChecklistDao checklistDao = new ChecklistDao();
        
        // Test save
        checklistDao.save(checklist);
        
        //Test get
        Checklist updateChecklist = checklistDao.get(3);
        System.out.println(updateChecklist);
//
//        // Test update
        String dateFormat1 = "2022-12-30 23:59";
        Date date1 = df.parse(dateFormat1);
        updateChecklist.setDate(date1);
        checklistDao.update(updateChecklist);

//        // Test delete
        checklistDao.delete(checklistDao.get(3));
    }
}
