/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.poc;

import com.mycompany.dcoffee.dao.CheckListItemDao;
import com.mycompany.dcoffee.model.CheckListItem;

/**
 *
 * @author PC Sakda
 */
public class TestCheckListItemDao {

    public static void main(String[] args) {
        CheckListItem checklistitem = new CheckListItem(0, "กัญชา", 20, "ตัน", 10);
        CheckListItemDao checklistitemDao = new CheckListItemDao();

//         Test save
        checklistitemDao.save(checklistitem);
        // Test delete
//        checklistitemDao.delete(checklistitemDao.get(8));
        // Test get
        CheckListItem updateCheckListItem = checklistitemDao.get(8);
        System.out.println(updateCheckListItem);
        
        // Test update
        updateCheckListItem.setName("Sakda");
        checklistitemDao.update(updateCheckListItem);
    }
}
