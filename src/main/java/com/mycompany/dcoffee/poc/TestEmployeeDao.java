/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.poc;

import com.mycompany.dcoffee.dao.EmployeeDao;
import com.mycompany.dcoffee.helper.DatabaseHelper;
import com.mycompany.dcoffee.model.Employee;

/**
 *
 * @author nasan
 */
public class TestEmployeeDao {
    public static void main(String[] args) {
        Employee employee = new Employee("สมชาย","ชายสมอน","0947748513","บางนา","somchai@gmail.com","158493648125",55);
        EmployeeDao employeeDao = new EmployeeDao();
        
        // Test save
        employeeDao.save(employee);
        // Test delete
        employeeDao.delete(employeeDao.get(4));
        // Test get
        Employee updateEmployee = employeeDao.get(4);
        System.out.println(updateEmployee);
        // Test update
        updateEmployee.setName("นายบาวง");
        employeeDao.update(updateEmployee);
        

    }
    
}
