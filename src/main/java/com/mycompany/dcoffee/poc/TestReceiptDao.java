/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.poc;

import com.mycompany.dcoffee.dao.ReceiptDao;
import com.mycompany.dcoffee.model.Receipt;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kitty
 */
public class TestReceiptDao {

    public static void main(String[] args) throws ParseException {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateFormat = "2024-05-05 18:00";
        Date date = df.parse(dateFormat);
        Receipt receipt = new Receipt(80,1,100,20,12,date);
        System.out.println(receipt);
        ReceiptDao receiptDao = new ReceiptDao();
//        receiptDao.save(receipt);
        //Test Get
        Receipt updateReceipt = receiptDao.get(4);
        System.out.println(updateReceipt);
//        //Test Upadate
        updateReceipt.setCash(5000);
        receiptDao.update(updateReceipt);
//          //Test delete
//       receiptDao.delete(receiptDao.get(7));
    }
}
