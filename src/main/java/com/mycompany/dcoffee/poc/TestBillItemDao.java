/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.poc;

import com.mycompany.dcoffee.dao.BillItemDao;
import com.mycompany.dcoffee.model.BillItem;

/**
 *
 * @author Acer
 */
public class TestBillItemDao {

    public static void main(String[] args) {
        BillItem billitem = new BillItem(2, 400, 1200);
        BillItemDao billitemDao = new BillItemDao();

//    save
        billitemDao.save(billitem);

//   Update
        BillItem updateBillItem = billitemDao.get(8);
        updateBillItem.setQuantity(3);
        billitemDao.update(updateBillItem);

        billitemDao.delete(billitemDao.get(8));

    }
}
