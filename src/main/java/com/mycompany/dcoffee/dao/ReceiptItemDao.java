/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.dao;

import com.mycompany.dcoffee.helper.DatabaseHelper;
import com.mycompany.dcoffee.model.Product;
import com.mycompany.dcoffee.model.ReceiptItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Melon
 */
public class ReceiptItemDao implements Dao<ReceiptItem> {

    @Override
    public ReceiptItem get(int id) {
        ReceiptItem item = null;
        String sql = "SELECT * FROM receipt_item WHERE receipt_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = ReceiptItem.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<ReceiptItem> getAll() {
        ArrayList<ReceiptItem> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptItem item = ReceiptItem.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ReceiptItem save(ReceiptItem obj) {
        String sql = "INSERT INTO receipt_item (receipt_item_amount, receipt_item_price, receipt_item_total)" + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getAmount());
            stmt.setDouble(2, obj.getPrice());
            stmt.setDouble(3, obj.getTotal());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;

    }

    @Override
    public ReceiptItem update(ReceiptItem obj) {
        return null;
    }

    @Override
    public int delete(ReceiptItem obj) {
        return -1;
    }

    @Override
    public List<ReceiptItem> getAll(String where, String order) {
        ArrayList<ReceiptItem> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptItem item = ReceiptItem.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

 

}
