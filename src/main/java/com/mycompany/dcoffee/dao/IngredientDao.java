/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.dao;

import com.mycompany.dcoffee.helper.DatabaseHelper;
import com.mycompany.dcoffee.model.Ingredient;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Acer
 */
public class IngredientDao implements Dao<Ingredient> {

    @Override
    public Ingredient get(int id) {
        Ingredient ingredient = null;
        String sql = "SELECT * FROM ingredient WHERE ingredient_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ingredient = Ingredient.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ingredient;
    }

    @Override
    public List<Ingredient> getAll() {
       ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM ingredient";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient ingredient = Ingredient.fromRS(rs);
                list.add(ingredient);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Ingredient save(Ingredient obj) {
        String sql = "INSERT INTO ingredient (ingredient_name,ingredient_quantity,ingredient_unit,ingredient_min)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getQuantity());
            stmt.setString(3, obj.getUnit());
            stmt.setInt(4, obj.getMin());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Ingredient update(Ingredient obj) {
         String sql = "UPDATE ingredient"
                + " SET ingredient_name = ?, ingredient_quantity = ?, ingredient_unit = ?, ingredient_min = ?"
                + " WHERE ingredient_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getQuantity());
            stmt.setString(3, obj.getUnit());
            stmt.setInt(4, obj.getMin());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Ingredient obj) {
        String sql = "DELETE FROM ingredient WHERE ingredient_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Ingredient> getAll(String where, String order) {
       ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM ingredient where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient ingredient = Ingredient.fromRS(rs);
                list.add(ingredient);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
