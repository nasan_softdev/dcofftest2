/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.service;

import com.mycompany.dcoffee.dao.UserDao;
import com.mycompany.dcoffee.model.User;

/**
 *
 * @author nasan
 */
public class UserService {
    
        public static User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if (user != null && user.getUserpassword().equals(password)) {
            return user;
        }
        return null;
    }
}
