/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nasan
 */
public class Employee {

    private int id;
    private String name;
    private String surname;
    private String phone;
    private String address;
    private String email;
    private String idNumber;
    private double hourlyRate;

    public Employee(int id, String name, String surname, String phone, String address, String email, String idNumber, double hourlyRate) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.address = address;
        this.email = email;
        this.idNumber = idNumber;
        this.hourlyRate = hourlyRate;
    }

    public Employee(String name, String surname, String phone, String address, String email, String idNumber, double hourlyRate) {
        this.id = -1;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.address = address;
        this.email = email;
        this.idNumber = idNumber;
        this.hourlyRate = hourlyRate;
    }

    public Employee() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", surname=" + surname + ", phone=" + phone + ", address=" + address + ", email=" + email + ", idNumber=" + idNumber + ", hourlyRate=" + hourlyRate + '}';
    }



    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("employee_id"));
            employee.setName(rs.getString("employee_name"));
            employee.setSurname(rs.getString("employee_surename"));
            employee.setPhone(rs.getString("employee_phone"));
            employee.setAddress(rs.getString("employee_address"));
            employee.setEmail(rs.getString("employee_email"));
            employee.setIdNumber(rs.getString("employee_idNumber"));
            employee.setHourlyRate(rs.getDouble("employee_hourly_rate"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }

}
