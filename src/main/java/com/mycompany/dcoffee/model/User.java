/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Melon
 */
public class User {

    private int id;
    private String username;
    private String userpassword;
    private int role;
    
    public User(int id, String username, String userpassword,int role) {
        this.id = id;
        this.username = username;
        this.userpassword = userpassword;
        this.role = role;
    }
    public User(String username, String userpassword,int role) {
        this.id = -1;
        this.username = username;
        this.userpassword = userpassword;
        this.role = role;
    }
    public User() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpassword() {
        return userpassword;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", userpassword=" + userpassword + ", role=" + role + '}';
    }
    

    public static User fromRS(ResultSet rs) {
       User user = new User();
        try {
            user.setId(rs.getInt("user_id"));
            user.setUsername(rs.getString("user_name"));
            user.setUserpassword(rs.getString("user_password"));
            user.setRole(rs.getInt("user_role"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } 
        return user;
    }

    
}
