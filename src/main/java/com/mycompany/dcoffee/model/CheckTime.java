/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pinkz7_
 */
public class CheckTime {

    private int id;
    private Date in;
    private Date out;
    private int hour;
    private String payment;

    public CheckTime(int id, Date in, Date out, int hour, String payment) {
        this.id = id;
        this.in = in;
        this.out = out;
        this.hour = hour;
        this.payment = payment;
    }

    public CheckTime() {
        this.id=-1;
    }

    public CheckTime(Date in, Date out, int hour, String payment) {
        this.id=-1;
        this.in = in;
        this.out = out;
        this.hour = hour;
        this.payment = payment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getIn() {
        return in;
    }

    public void setIn(Date in) {
        this.in = in;
    }

    public Date getOut() {
        return out;
    }

    public void setOut(Date out) {
        this.out = out;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    @Override
    public String toString() {
        return "CheckTime{" + "id=" + id + ", in=" + in + ", out=" + out + ", hour=" + hour + ", payment=" + payment + '}';
    }
    
    public static CheckTime fromRS(ResultSet rs) {
        CheckTime checktime = new CheckTime();
        try {
            checktime.setId(rs.getInt("check_time_id"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String in = rs.getString("check_time_in");
            String out = rs.getString("check_time_out");
            try {
                checktime.setIn(df.parse(in));
                checktime.setOut(df.parse(out));
            } catch (ParseException ex) {
                Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            }
            checktime.setHour(rs.getInt("check_time_hour"));
            checktime.setPayment(rs.getString("check_time_payment"));

        } catch (SQLException ex) {
            Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checktime;
    }

}
