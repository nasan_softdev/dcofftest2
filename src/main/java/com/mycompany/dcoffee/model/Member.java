/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pinkz7_
 */
public class Member {
    private int id;
    private String name;
    private String suername;
    private String phone;
    private int point;

    public Member(int id, String name, String suername, String phone, int point) {
        this.id = id;
        this.name = name;
        this.suername = suername;
        this.phone = phone;
        this.point = point;
    }

    public Member(String name, String suername, String phone, int point) {
        this.id = -1;
        this.name = name;
        this.suername = suername;
        this.phone = phone;
        this.point = point;
    }
    
    public Member(){
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSuername() {
        return suername;
    }

    public void setSuername(String suername) {
        this.suername = suername;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "Member{" + "id=" + id + ", name=" + name + ", suername=" + suername + ", phone=" + phone + ", point=" + point + '}';
    }
    
    public static Member fromRS(ResultSet rs) {
        Member member = new Member();
        try {
            member.setId(rs.getInt("member_id"));
            member.setName(rs.getString("member_name"));
            member.setSuername(rs.getString("member_surename"));
            member.setPhone(rs.getString("member_phone"));
            member.setPoint(rs.getInt("member_point"));
        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } 
        return member;
    }
    
    
    
    
    
}
