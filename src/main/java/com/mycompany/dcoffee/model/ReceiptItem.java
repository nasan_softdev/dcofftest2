/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Melon
 */
public class ReceiptItem {
    private  int id;
    private int amount;
    private double price;
    private double total;

    public ReceiptItem(int id, int amount, double price, double total) {
        this.id = id;
        this.amount = amount;
        this.price = price;
        this.total = total;
    }
    
    public ReceiptItem( int amount, double price, double total) {
        this.id = -1;
        this.amount = amount;
        this.price = price;
        this.total = total;
    }
    
    public ReceiptItem() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
      public static ReceiptItem fromRS(ResultSet rs) {
        ReceiptItem receiptItem = new ReceiptItem();
        try {
            receiptItem.setId(rs.getInt("receipt_item_id"));
            receiptItem.setAmount(rs.getInt("receipt_item_amount"));
            receiptItem.setPrice(rs.getDouble("receipt_item_price"));
            receiptItem.setTotal(rs.getDouble("receipt_item_total"));

        } catch (SQLException ex) {
            Logger.getLogger(ReceiptItem.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receiptItem;
    }


    @Override
    public String toString() {
        return "receiptitem{" + "id=" + id + ", amount=" + amount + ", price=" + price + ", total=" + total + '}';
    }

   
    
    

}
