/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PC Sakda
 */
public class Checklist {

    private int id;
    private Date date;

    public Checklist(int id, Date date) {
        this.id = id;
        this.date = date;
    }

    public Checklist(Date date) {
        this.id = -1;
        this.date = date;
    }

    public Checklist() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Checklist{" + "id=" + id + ", date=" + date + '}';
    }

    public static Checklist fromRS(ResultSet rs) {
        Checklist checklist = new Checklist();
        try {
            checklist.setId(rs.getInt("checklist_id"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String date = rs.getString("check_list_date");
            try {
                checklist.setDate(df.parse(date));
            } catch (ParseException ex) {
                Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checklist;
    }
}
