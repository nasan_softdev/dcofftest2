/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nasan
 */
public class Payroll {

    private int id;
    private Date date;
    private double amount;
    private double deduct;
    private double total;

    public Payroll(int id, Date date, double amount, double deduct, double total) {
        this.id = id;
        this.date = date;
        this.amount = amount;
        this.deduct = deduct;
        this.total = total;
    }

    public Payroll(Date date, double amount, double deduct, double total) {
        this.id = -1;
        this.date = date;
        this.amount = amount;
        this.deduct = deduct;
        this.total = total;
    }

    public Payroll() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getDeduct() {
        return deduct;
    }

    public void setDeduct(double deduct) {
        this.deduct = deduct;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Payroll{" + "id=" + id + ", date=" + date + ", amount=" + amount + ", deduct=" + deduct + ", total=" + total + '}';
    }

    public static Payroll fromRS(ResultSet rs) {
        Payroll payroll = new Payroll();
        try {
            payroll.setId(rs.getInt("payroll_id"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String date = rs.getString("payroll_date");
            try {
                payroll.setDate(df.parse(date));
            } catch (ParseException ex) {
                Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            }
            payroll.setAmount(rs.getDouble("payroll_amount"));
            payroll.setDeduct(rs.getDouble("payroll_deduct"));
            payroll.setTotal(rs.getDouble("payroll_total"));

        } catch (SQLException ex) {
            Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return payroll;
    }
}
