/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kitty
 */
public class Receipt {

    private int id;
    private double discount;
    private double total;
    private double cash;
    private double change;
    private int queue;
    private Date timestamp;
    

    public Receipt(int id, double total, double discount, double cash, double change, int queue, Date timestamp) {
        this.id = id;
        this.discount = discount;
        this.total = total;
        this.cash = cash;
        this.change = change;
        this.queue = queue;
        this.timestamp = timestamp;
     
    }

    public Receipt(double total, double discount, double cash, double change, int queue, Date timestamp) {
        this.id = -1;
        this.discount = discount;
        this.total = total;
        this.cash = cash;
        this.change = change;
        this.queue = queue;
        this.timestamp = timestamp;
    }

    public Receipt(Date timestamp) {
        this.timestamp = timestamp;

    }

    public Receipt() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public int getQueue() {
        return queue;
    }

    public void setQueue(int queue) {
        this.queue = queue;
    }

    public Date getDate() {
        return timestamp;
    }

    public void setDate(Date timestamp) {
        this.timestamp = timestamp;
    }

    public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("receipt_id"));
            receipt.setTotal(rs.getDouble("receipt_total"));
            receipt.setDiscount(rs.getDouble("receipt_discount"));
            receipt.setCash(rs.getDouble("receipt_cash"));
            receipt.setChange(rs.getDouble("receipt_change"));
            receipt.setQueue(rs.getInt("receipt_queue_num"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String date = rs.getString("receipt_timestamp");
            try {
                receipt.setDate(df.parse(date));
            } catch (ParseException ex) {
                Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receipt;

    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", discount=" + discount + ", total=" + total + ", cash=" + cash + ", change=" + change + ", queue=" + queue + ", timestamp=" + timestamp + '}';
    }
  

}
