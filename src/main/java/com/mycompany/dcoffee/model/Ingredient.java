/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class Ingredient {
    private int id;
    private String name;
    private double quantity;
    private String unit;
    private int min;

    public Ingredient(int id, String name, double quantity, String unix, int min) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.unit = unix;
        this.min = min;
    }
    public Ingredient(String name, double quantity, String unix, int min) {
        this.id = -1;
        this.name = name;
        this.quantity = quantity;
        this.unit = unix;
        this.min = min;
    }

    public Ingredient() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unix) {
        this.unit = unix;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    @Override
    public String toString() {
        return "Ingredient{" + "id=" + id + ", name=" + name + ", quantity=" + quantity + ", unix=" + unit + ", min=" + min + '}';
    }
    public static Ingredient fromRS(ResultSet rs) {
        Ingredient ingredient = new Ingredient();
        try {
            ingredient.setId(rs.getInt("ingredient_id"));
            ingredient.setName(rs.getString("ingredient_name"));
            ingredient.setQuantity(rs.getDouble("ingredient_quantity"));
            ingredient.setUnit(rs.getString("ingredient_unit"));
            ingredient.setMin(rs.getInt("ingredient_min"));
        } catch (SQLException ex) {
            Logger.getLogger(Ingredient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } 
        return ingredient;
    }
    
}
