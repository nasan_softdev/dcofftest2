/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class BillItem {
    private int id;
    private int quantity;
    private double priceperunit;
    private double totalprice;

    public BillItem(int id, int quantity, double priceperunit, double totalprice) {
        this.id = id;
        this.quantity = quantity;
        this.priceperunit = priceperunit;
        this.totalprice = totalprice;
    }
    public BillItem( int quantity, double priceperunit, double totalprice) {
        this.id = -1;
        this.quantity = quantity;
        this.priceperunit = priceperunit;
        this.totalprice = totalprice;
    }

    public BillItem() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPriceperunit() {
        return priceperunit;
    }

    public void setPriceperunit(double priceperunit) {
        this.priceperunit = priceperunit;
    }

    public double getTotalPrice() {
        return totalprice;
    }

    public void setTotalPrice(double totalprice) {
        this.totalprice = totalprice;
    }

    @Override
    public String toString() {
        return "BillItem{" + "id=" + id + ", quantity=" + quantity + ", priceperunit=" + priceperunit + ", totalprice=" + totalprice + '}';
    }
    public static BillItem fromRS(ResultSet rs) {
        BillItem billitem = new BillItem();
        try {
            billitem.setId(rs.getInt("bill_item_id"));
            billitem.setQuantity(rs.getInt("bill_item_quantity"));
            billitem.setPriceperunit(rs.getDouble("bill_item_price_per_unit"));
            billitem.setTotalPrice(rs.getDouble("bill_item_total_price"));
            
        } catch (SQLException ex) {
            Logger.getLogger(BillItem.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } 
        return billitem;
    }
    
    
}
